#!/usr/bin/perl

# This file is part of Mana.
#
# Mana is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Koha is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Koha; if not, see <http://www.gnu.org/licenses>.

use Modern::Perl;

use Dancer2;
use Dancer2::Plugin::Database;

# Create table store;
my $sth = database->prepare(
    'CREATE TABLE IF NOT EXISTS store (
	variable varchar(255) not null,
	value varchar(255) not null
    )'
);
$sth->execute();

my $version = '1.0';
if ( CheckVersion( $version ) ) {
    SetVersion( $version );
    print "Upgrade to $version done. Add updatedatabase ability.\n"
}

=head2 CheckVersion

Check whether a given update should be run when passed the proposed version
number. The update will always be run if the proposed version is greater
than the current database version. 

=cut

sub CheckVersion {
    my ($proposed_version) = @_;

    my $DBversion;

    $DBversion = database->quick_select('store',
	{ variable => 'version'}, { columns => ['value'] } ) || '0.0';

    if ( $DBversion < $proposed_version ) {
        return 1;
    }

    return 0;
}

=head2 SetVersion

Set database version

=cut

sub SetVersion {
    my $new_version = shift;

    my $DBversion = database->quick_select('store',
	{ variable => 'version'}, { columns => ['value'] } ) || '';

    if ( $DBversion ) {
	database->quick_update('store', { variable => 'version' }, { value => $new_version });
	return;
    }

    database->quick_insert('store', { variable => 'version', value => $new_version });
}

