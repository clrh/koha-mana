SET FOREIGN_KEY_CHECKS=0;

--
-- Table structure for table subscription
--

DROP TABLE IF EXISTS user;
CREATE TABLE user(
    id int(4) NOT NULL auto_increment,
    email VARCHAR(255),
    login VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    PRIMARY KEY(id)
);

DROP TABLE IF EXISTS resource_comment;
CREATE TABLE resource_comment(
    id int(11) NOT NULL auto_increment,
    message VARCHAR(35),
    resource_id int(11),
    resource_type VARCHAR(40),
    nb int(5),
    creationdate date,
    PRIMARY KEY(id)
);

DROP TABLE IF EXISTS librarian;
CREATE TABLE librarian(
    id VARCHAR(32) PRIMARY KEY,
    email VARCHAR(100) NOT NULL,
    firstname VARCHAR(20) NOT NULL,
    lastname VARCHAR(50) NOT NULL,
    creationdate date NOT NULL,
    activationdate date DEFAULT NULL,
    lastshare date DEFAULT NULL,
    nbaccess integer DEFAULT 0
);

DROP TABLE IF EXISTS reading_pair;
DROP TABLE IF EXISTS document;
CREATE TABLE document (
    id integer auto_increment PRIMARY KEY,
    isbn varchar(17),
    issn varchar(10),
    ean varchar(13),
    exportemail varchar(100),
    securitytoken varchar(32)
);

CREATE TABLE reading_pair(
    id integer PRIMARY KEY AUTO_INCREMENT,
    documentid1 integer,
    documentid2 integer,
    last_insert date NOT NULL,
    nb integer DEFAULT '0',
    timestamp timestamp DEFAULT CURRENT_TIMESTAMP,
    nb_this_month date,
    exportemail varchar(100),
    securitytoken varchar(32),
    FOREIGN KEY (documentid1) REFERENCES document(id),
    FOREIGN KEY (documentid2) REFERENCES document(id)
);

DROP TABLE IF EXISTS subscription;
CREATE TABLE subscription ( -- information related to the subscription
    id int(11) NOT NULL auto_increment, -- unique key for this subscription
    -- subscription info
    title mediumtext, -- in biblio table in KOHA : title (without the subtitle) from the MARC record (245$a in MARC21)
    issn mediumtext, -- in biblioitem table in KOHA : ISSN (MARC21 022$a)
    ean varchar(13) default NULL,-- in biblioitem table in KOHA
    publishercode varchar(255) default NULL, -- in biblioitem table in KOHA : publisher (MARC21 260$b)
    -- subscription_frequency info --
    sfdescription TEXT NOT NULL,
    unit ENUM('day','week','month','year') DEFAULT NULL,
    unitsperissue INTEGER NOT NULL DEFAULT '1',
    issuesperunit INTEGER NOT NULL DEFAULT '1',
    -- subscription_numberpatterns info -- 
    label VARCHAR(255) NOT NULL,
    sndescription TEXT NOT NULL,
    numberingmethod VARCHAR(255) NOT NULL,
    label1 VARCHAR(255) DEFAULT NULL,
    add1 INTEGER DEFAULT NULL,
    every1 INTEGER DEFAULT NULL,
    whenmorethan1 INTEGER DEFAULT NULL,
    setto1 INTEGER DEFAULT NULL,
    numbering1 VARCHAR(255) DEFAULT NULL,
    label2 VARCHAR(255) DEFAULT NULL,
    add2 INTEGER DEFAULT NULL,
    every2 INTEGER DEFAULT NULL,
    whenmorethan2 INTEGER DEFAULT NULL,
    setto2 INTEGER DEFAULT NULL,
    numbering2 VARCHAR(255) DEFAULT NULL,
    label3 VARCHAR(255) DEFAULT NULL,
    add3 INTEGER DEFAULT NULL,
    every3 INTEGER DEFAULT NULL,
    whenmorethan3 INTEGER DEFAULT NULL,
    setto3 INTEGER DEFAULT NULL,
    numbering3 VARCHAR(255) DEFAULT NULL,
    -- mana data --
    nbofusers INTEGER DEFAULT NULL, -- number of users using this model
    lastimport DATE NOT NULL, -- date of the last import
    creationdate DATE NOT NULL, -- date of the creation
    exportemail VARCHAR(255) DEFAULT NULL,
    kohaversion VARCHAR(255) DEFAULT NULL,
    language varchar(255) default NULL, -- language of the description and label of numbering pattern and frequency
    PRIMARY KEY  (id)
);  -- ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS report;
CREATE TABLE report ( -- information related to the report
    id int(11) NOT NULL auto_increment,
    -- report infos
    savedsql longtext NOT NULL,
    report_name varchar(255) NOT NULL,
    notes VARCHAR(255),
    type VARCHAR(255),
    -- mana infos
    nbofusers INTEGER DEFAULT NULL, -- number of users using this model
    lastimport DATE NOT NULL, -- date of the last import
    creationdate DATE NOT NULL, -- date of the creation
    exportemail VARCHAR(255) DEFAULT NULL,
    kohaversion VARCHAR(255) DEFAULT NULL,
    language varchar(255) default NULL, -- language of the description and label of numbering pattern and frequency
    PRIMARY KEY(id)
);

--
-- Table structure for table comment
--

DROP TABLE IF EXISTS commentary;
CREATE TABLE commentary ( -- information related to the comment
  id int(11) NOT NULL auto_increment, -- unique key for this comment
  contentid int(11) not null, -- link with the id of the commented content
  contententity enum('subscription','subscription_numberpatterns','subscription_frequencies') not null, -- link with the name of the entity of the commented content
  commentary mediumtext, -- the commentary
  email varchar(255) default null, -- email of the personn who posted the comment
  posteddate date not null,-- date when the comment was posted
  deprecated integer default null,
  PRIMARY KEY  (id)
);  -- ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS store;
CREATE TABLE store (
  variable varchar(255) not null,
  value varchar(255) not null
);

SET FOREIGN_KEY_CHECKS=1;
