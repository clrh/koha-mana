#! /usr/bin/perl -w

use Modern::Perl;

use Mana;
use Test::More tests => 5;
use Plack::Test;
use HTTP::Request::Common;
use Dancer2;
use Dancer2::Plugin::Database;
use Dancer::Logger;
use Data::Dumper;
use Dancer2::Plugin::REST;
use Dancer2::Serializer::JSON;
use utf8;

my $app = Mana->to_app;

my $test = Plack::Test->create($app);

database->{AutoCommit} = 0;
database->quick_delete("subscription", 1);
database->quick_delete("librarian", 1);
database->quick_insert("librarian", { id => "1234", email =>'xx@xx.com', firstname => 'Pierre', lastname => 'Dupont', creationdate => '2017-04-18', activationdate => '2017-04-18'});


my $newSubscription;
$newSubscription->{title} ="this is a title" ;
$newSubscription->{issn} ="this is an issn" ;
$newSubscription->{ean} ="ean" ;
$newSubscription->{publishercode} ="this is a publishercode" ;
$newSubscription->{securitytoken} ="1234" ;
my $newSubscriptionShort;
%$newSubscriptionShort = %$newSubscription;

$newSubscription->{numberingmethod} = "this is a numbering method" ;
$newSubscription->{sfdescription} = "this is a description" ;
$newSubscription->{unit} = "day" ;
$newSubscription->{exportemail} = 'XX@xx.com' ;
$newSubscription->{kohaversion} = '0.0' ;
$newSubscription->{language} = 'ouzbek' ;
$newSubscription->{label1} = "this is the label1" ;
$newSubscription->{add1} = 1;
$newSubscription->{every1} = 1;
$newSubscription->{whenmorethan1} = 1 ;
$newSubscription->{setto1} = 1 ;
$newSubscription->{numbering1} = "this is a numbering" ;
$newSubscription->{label2} = "this is the label2" ;
$newSubscription->{add2} = 2;
$newSubscription->{every2} = 2;
$newSubscription->{whenmorethan2} = 2 ;
$newSubscription->{setto2} = 2 ;
$newSubscription->{numbering2} = "this is a numbering2" ;
$newSubscription->{label3} = "this is the label3" ;
$newSubscription->{add3} = 3;
$newSubscription->{every3} = 3;
$newSubscription->{whenmorethan3} = 3 ;
$newSubscription->{setto3} = 3 ;
$newSubscription->{numbering3} = "this is a numbering3" ;

my $newSubscription2;
$newSubscription2->{title} ="this is a second title" ;
$newSubscription2->{issn} ="this is a secondn issn" ;
$newSubscription2->{ean} ="ean2" ;
$newSubscription2->{publishercode} ="this is a second publishercode" ;
$newSubscription2->{securitytoken} ="1234" ;
my $newSubscription2Short;
%$newSubscription2Short = %$newSubscription;

$newSubscription2->{numberingmethod} = "this is a second numbering method" ;
$newSubscription2->{sfdescription} = "this is a second description" ;
$newSubscription2->{unit} = "day" ;
$newSubscription2->{exportemail} = 'XX@xx.com' ;
$newSubscription2->{kohaversion} = '0.0' ;
$newSubscription2->{language} = 'ouzbek' ;
$newSubscription2->{label1} = "this is the label1" ;
$newSubscription2->{add1} = 1;
$newSubscription2->{every1} = 1;
$newSubscription2->{whenmorethan1} = 1 ;
$newSubscription2->{setto1} = 1 ;
$newSubscription2->{numbering1} = "this is a second numbering" ;
$newSubscription2->{label2} = "this is the label2" ;
$newSubscription2->{add2} = 2;
$newSubscription2->{every2} = 2;
$newSubscription2->{whenmorethan2} = 2 ;
$newSubscription2->{setto2} = 2 ;
$newSubscription2->{numbering2} = "this is a second numbering2" ;
$newSubscription2->{label3} = "this is the label3" ;
$newSubscription2->{add3} = 3;
$newSubscription2->{every3} = 3;
$newSubscription2->{whenmorethan3} = 3 ;
$newSubscription2->{setto3} = 3 ;
$newSubscription2->{numbering3} = "this is a second numbering3" ;

my $newSubscription3;
$newSubscription3->{title} ="this is a third title" ;
$newSubscription3->{issn} ="this is a thirdn issn" ;
$newSubscription3->{ean} ="ean3" ;
$newSubscription3->{publishercode} ="this is a third publishercode" ;
$newSubscription3->{securitytoken} ="1234" ;
my $newSubscription3Short;
%$newSubscription3Short = %$newSubscription;

$newSubscription3->{numberingmethod} = "this is a third numbering method" ;
$newSubscription3->{sfdescription} = "this is a third description" ;
$newSubscription3->{unit} = "day" ;
$newSubscription3->{exportemail} = 'XX@xx.com' ;
$newSubscription3->{kohaversion} = '0.0' ;
$newSubscription3->{language} = 'ouzbek' ;
$newSubscription3->{label1} = "this is the label1" ;
$newSubscription3->{add1} = 1;
$newSubscription3->{every1} = 1;
$newSubscription3->{whenmorethan1} = 1 ;
$newSubscription3->{setto1} = 1 ;
$newSubscription3->{numbering1} = "this is a third numbering" ;
$newSubscription3->{label2} = "this is the label2" ;
$newSubscription3->{add2} = 2;
$newSubscription3->{every2} = 2;
$newSubscription3->{whenmorethan2} = 2 ;
$newSubscription3->{setto2} = 2 ;
$newSubscription3->{numbering2} = "this is a third numbering2" ;
$newSubscription3->{label3} = "this is the label3" ;
$newSubscription3->{add3} = 3;
$newSubscription3->{every3} = 3;
$newSubscription3->{whenmorethan3} = 3 ;
$newSubscription3->{setto3} = 3 ;
$newSubscription3->{numbering3} = "this is a third numbering3" ;




my @attributelist = keys %$newSubscription;
push @attributelist, 'nbofusers';


subtest 'critical cases for Share function' => sub {
    plan tests => 3;
    
    my $temp = delete $newSubscription->{title};
    my $newJsonSubscription = to_json($newSubscription, { utf8 => 1 });
    my $nbbeforeinsert = database->quick_count("subscription", 1);
    my $resJson = $test->request( POST '/subscription.json', Content => $newJsonSubscription );
    my $res = from_json( $resJson->content );

    subtest 'attribute title missing' => sub {
        plan tests => 3;
        is( $resJson->code, 406, "right error code");
        ok( $res->{msg} =~ /Invalid input on/, "propper error message");

        #nothing were added
        is( database->quick_count( "subscription", 1 ), $nbbeforeinsert, "database is the same after error");
    };

    $newSubscription->{title} = $temp;
    $temp = delete $newSubscription->{publishercode};
    $newJsonSubscription = to_json($newSubscription, { utf8 => 1 });
    $nbbeforeinsert = database->quick_count("subscription", 1);
    $resJson = $test->request( POST '/subscription.json', Content => $newJsonSubscription );

    subtest 'attribute publishercode missing' => sub {
        is( $resJson->code, 406, "right error code");
        $res = from_json( $resJson->content );
        ok( $res->{msg} =~ /Invalid input on/, "propper error message");

        #nothing were added
        is( database->quick_count( "subscription", 1 ), $nbbeforeinsert, "database is the same after error");
    };
    $newSubscription->{publishercode} = $temp;

    $temp = delete $newSubscription->{language};
    $newJsonSubscription = to_json($newSubscription, { utf8 => 1 });
    $nbbeforeinsert = database->quick_count("subscription", 1);
    $resJson = $test->request( POST '/subscription.json', Content => $newJsonSubscription );

    subtest 'attribute language missing' => sub {
        is( $resJson->code, 406, "right error code");
        $res = from_json( $resJson->content );
        ok( $res->{msg} =~ /Invalid input on/, "propper error message");

        #nothing were added
        is( database->quick_count( "subscription", 1 ), $nbbeforeinsert, "database is the same after error");
    };
    $newSubscription->{language} = $temp;


};


subtest 'Share function' => sub {
    plan tests => 3;


    my $newJsonSubscription = to_json($newSubscription, { utf8 => 1 });
    my $nbbeforeinsert = database->quick_count("subscription", 1);
    my $resJson = $test->request( POST '/subscription.json', Content => $newJsonSubscription );
    my $resdb = database->quick_select("subscription", $newSubscriptionShort);
    my $nbafterinsert = database->quick_count("subscription", 1);
    is( $nbafterinsert, $nbbeforeinsert + 1, "added something in database");

    foreach my $key ( keys %{$resdb} ){
        delete $resdb->{$key} unless grep {$key eq $_} @attributelist;
    }

        #test about expected values of the fields
    $newSubscription->{nbofusers} = 1;
    is_deeply( $resdb, $newSubscription,  "nb of users correctly loaded");

        #test about the error returned by function
    my $res = from_json( $resJson->content );
    is( $res->{'error'}, undef , "no error returned");
   
};



subtest "Get by id Route" => sub {
    plan tests => 2;
    my $id = database->quick_select("subscription", $newSubscriptionShort)->{id};

    my $resJson = $test->request( GET "/subscription/$id.json" );
    my $res = from_json( $resJson->content );
    my $resdata = $res->{data};

    foreach my $key ( keys %{$resdata} ){
        delete $resdata->{$key} unless grep {$key eq $_} @attributelist;
    }

        #test about expected values of the fields
    $resdata->{securitytoken} = $newSubscription->{securitytoken};
    is_deeply( $resdata, $newSubscription,  "get the right resource with id");

        #test about the error returned by function
    is( $res->{'error'}, undef , "no error returned");
 
};

subtest "Search engine" => sub {
    plan tests => 3;
 
    my $newJsonSubscription2 = to_json($newSubscription2, { utf8 => 1 });
    my $jsonresult2 = $test->request( POST '/subscription.json', Content => $newJsonSubscription2 );
    my $result = from_json($jsonresult2->content);
    my $id = database->quick_select("subscription", $newSubscriptionShort)->{id};

    my $newJsonSubscription3 = to_json($newSubscription3, { utf8 => 1 });
    $test->request( POST '/subscription.json', Content => $newJsonSubscription3 );

    my $resJson = $test->request( GET "/subscription.json?title=third");
    my $res = from_json( $resJson->content );
    my $resdata = $res->{data};



    foreach my $key ( keys %{$resdata->[0]} ){
        delete $resdata->[0]->{$key} unless grep {$key eq $_} @attributelist;
    }

    $newSubscription3->{nbofusers} = 1;
    $newSubscription2->{nbofusers} = 1;
    is_deeply($resdata->[0], $newSubscription3, "research successful");
    is(scalar keys @$resdata, 1, "only the expected search");
        #test about the error returned by function
    is( $res->{'error'}, undef , "no error returned");
 
};


subtest "increment field" => sub {
    plan tests => 2;
    
    my $id = database->quick_select("subscription", $newSubscription)->{id};
    my $resJson = $test->request( POST "/subscription/$id.json/increment/nbofusers?securitytoken=1234");

    my $incrementedSubscriptionInDB = database->quick_select("subscription", { id => $id });
    is( $incrementedSubscriptionInDB->{nbofusers}, 2, "correctly incremented" ); 

    $resJson = $test->request( POST "/subscription/$id.json/increment/nbofusers?step=2&securitytoken=1234");
    $incrementedSubscriptionInDB = database->quick_select("subscription", { id => $id } );
    is( $incrementedSubscriptionInDB->{nbofusers}, 4, "correctly incremented" );

};


eval{ database->rollback; };
database->{AutoCommit} = 1;
1;
