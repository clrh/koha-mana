#! /usr/bin/perl -w

use Modern::Perl;

use Mana;
use Test::More tests => 1;
use Plack::Test;
use HTTP::Request::Common;
use Dancer2;
use Dancer2::Plugin::Database;
use Dancer::Logger;
use Data::Dumper;
use Dancer2::Plugin::REST;
use Dancer2::Serializer::JSON;
use utf8;

my $app = Mana->to_app;

my $test = Plack::Test->create($app);

database->{AutoCommit} = 0;
database->quick_delete("report", 1);
database->quick_insert("librarian", { id => 1234, email => 'xx.xx@xx.com', firstname => 'Nathalie', lastname => 'Portman', creationdate => '2017-04-19', activationdate => '2017-04-19'});


my $newReport2;
$newReport2->{savedsql} ="Select car from parking" ;
$newReport2->{report_name} = "parking" ;
$newReport2->{notes} = "Count cars from parking";
$newReport2->{type} = "carsCounter";
$newReport2->{securitytoken} = "1234";
$newReport2->{report_group} = "carParking";
$newReport2->{language} = "FR";

my $newJsonReport2 = to_json($newReport2, { utf8 => 1 });
$test->request( POST '/report.json', Content => $newJsonReport2 );

my $id = database->quick_select("report", 1)->{id};

my $newSubscription;
$newSubscription->{title} ="this is a title" ;
$newSubscription->{issn} ="this is an issn" ;
$newSubscription->{ean} ="ean" ;
$newSubscription->{publishercode} = "this is a publishercode" ;
$newSubscription->{securitytoken} = '1234';
my $newSubscriptionShort;
%$newSubscriptionShort = %$newSubscription;

$newSubscription->{numberingmethod} = "this is a numbering method" ;
$newSubscription->{sfdescription} = "this is a description" ;
$newSubscription->{unit} = "day" ;
$newSubscription->{exportemail} = 'XX@xx.com' ;
$newSubscription->{kohaversion} = '0.0' ;
$newSubscription->{language} = 'ouzbek' ;
$newSubscription->{label1} = "this is the label1" ;
$newSubscription->{add1} = 1;
$newSubscription->{every1} = 1;
$newSubscription->{whenmorethan1} = 1 ;
$newSubscription->{setto1} = 1 ;
$newSubscription->{numbering1} = "this is a numbering" ;
$newSubscription->{label2} = "this is the label2" ;
$newSubscription->{add2} = 2;
$newSubscription->{every2} = 2;
$newSubscription->{whenmorethan2} = 2 ;
$newSubscription->{setto2} = 2 ;
$newSubscription->{numbering2} = "this is a numbering2" ;
$newSubscription->{label3} = "this is the label3" ;
$newSubscription->{add3} = 3;
$newSubscription->{every3} = 3;
$newSubscription->{whenmorethan3} = 3 ;
$newSubscription->{setto3} = 3 ;
$newSubscription->{numbering3} = "this is a numbering3" ;

my $newJsonSubscription = to_json($newSubscription, { utf8 => 1 });
$test->request( POST '/subscription.json', Content => $newJsonSubscription );
my $subscriptionid = database->quick_select("subscription", 1)->{id};




my $newResource_Comment;
$newResource_Comment->{message} ="This is a message" ;
$newResource_Comment->{resource_id} = $id;
$newResource_Comment->{resource_type} = "report";
$newResource_Comment->{nb} = 1;
$newResource_Comment->{securitytoken} = '1234';
$newResource_Comment->{exportemail} = 'xx.xx@xx.com';

my $newJsonResource_Comment = to_json($newResource_Comment, { utf8 => 1 });
my $resJson = $test->request( POST '/report.json', Content => $newJsonResource_Comment );

subtest 'Delete a librarian' => sub { 
    plan tests => 4;
    my $nbSub = database->quick_count('subscription', {securitytoken => '1234'} );
    my $nbRep = database->quick_count('report', {securitytoken => '1234'} );
    my $nbRes = database->quick_count('resource_comment', {securitytoken => '1234'} );
    my $nbRev = database->quick_count('review', {securitytoken => '1234'} );


    Mana::Resource::deleteResource( 'librarian', '1234' );

    my $nbSubafter = database->quick_count('subscription', {securitytoken => 'ANONYMOUS_TOKEN'} );
    my $nbRepafter  = database->quick_count('report', {securitytoken => 'ANONYMOUS_TOKEN'} );
    my $nbResafter  = database->quick_count('resource_comment', {securitytoken => 'ANONYMOUS_TOKEN'} );
    my $nbRevafter  = database->quick_count('review', {securitytoken => 'ANONYMOUS_TOKEN'} );

    is( $nbSub, $nbSubafter, 'subscription updated successfuly');
    is( $nbRep, $nbRepafter, 'report updated successfuly');
    is( $nbRes, $nbResafter, 'resource_comment updated successfuly');
    is( $nbRev, $nbRevafter, 'review updated successfuly');
};


eval{ database->rollback; };
database->{AutoCommit} = 1;
1;
