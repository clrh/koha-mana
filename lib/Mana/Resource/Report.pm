package Mana::Resource::Report;

use Dancer2;
use Dancer2::Plugin::Database;
use Mana::Resource;
use vars qw( @ISA ); @ISA = qw( Mana::Resource );

use Modern::Perl;


our @searchable_field = (
    'savedsql', 'report_name', 'report_group', 'notes', 'id', 'type', 'language'
);

our @required_entries  = (
    'savedsql', 'report_name', 'report_group', 'notes', 'language'
);

sub getValidEntries {
    return @required_entries;
}

our $resource_name = 'report';
sub getResourceName { return $resource_name };


sub dataInit {
    my $self = shift;
    my $content = shift;
    $content->{lastimport} = DateTime->now->ymd;
    $content->{creationdate} = DateTime->now->ymd;
    $content->{nbofusers} = 1;
    return $content;
}

sub specificSearch {
    my ($self, $params) = @_;
    my $query = "SELECT * FROM report WHERE 1";
    my @keys;
    my @values;
    my @words;
    if ($params->{query}){
        @words = split / /, $params->{query};
    }
    my @queryperword;
    foreach my $word (@words){
        foreach my $field (@searchable_field){
            push @keys, $field." LIKE ? ";
            push @values, "%$word%";
        }
        push @queryperword, "( ". join(" OR ", @keys). " )";
        @keys=();
    }
    
    if ( scalar @queryperword ne 0 ) {
        $query = $query." AND ". join (" AND ", @queryperword);
    }
    my $sth = database->prepare($query);
    $sth->execute(@values);
    my $rows = $sth->fetchall_arrayref({});
    return $rows;
}

sub resourceAlreadyExists {
    my ($self, $content) = @_;
    my $content_to_check;
    $content_to_check->{ savedsql } = $content->{ savedsql };
    if ( my $existing = database->quick_select('report', $content_to_check) ) {
        return $existing->{id};
    }
}

1;

