#!/usr/bin/perl

# This file is part of Mana.
#
# Mana is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Koha is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Koha; if not, see <http://www.gnu.org/licenses>.

use Modern::Perl;


use Dancer2;
use Dancer2::Serializer::JSON;

use Mana::Resource;
use Dancer2::Plugin::Database;

use Getopt::Long;

=head1 NAME

Reverse Import Resources - Cancel the modification performed by a given file

=head1 SYNOPSIS

PLACK_ENV=production reverse_import_resource.pl -f filename -r resourcename -l logfile

=head1 DESCRIPTION

This script reverses imports resources from a file stored as JSON which 2 arguments:
- securitytoken: the security token of the sharer.
- [ resourcename ]: the resources which are about to be imported, the number of resources is not limited.

=head1 OPTIONS

=over

=item --filename

The name of the file you want to import in the database.

=item --resource

The name of the resource you want to import.

=item --logfile

The file where you want to store the logs ( default production.log ).

=back

=head1 RETURNED VALUE

Returns the string stored in the logs.

=cut

my $resourcename;
    #path is [mana path]/data/[resourcename]/
    #path for imported datas: [mana path]/data/[resourcename]/old/
my $nb = 0;
my $return;

my $filename;
my $logfile;
my $nbbeforeinsert = database->quick_count("reading_pair", 1);
my $autocommit;

my ($help, $language, $verbose);
GetOptions(
    'help|?'        => \$help,
    'language|l:s'  => \$language,
    'verbose|v'     => \$verbose,
    'filename|f:s'    => \$filename,
    'resource|r:s'    => \$resourcename,
    'logfile|l:s'       => \$logfile,
    'confirm'    => \$autocommit
) or pod2usage(2);


$logfile||=config->{ log_path }."import.log";

my $duration;
open(my $fh, '<:encoding(UTF-8)', $filename) or (die "Could not open file '$filename'");
my $line;
print "total,duration,timeper1000input,time per input,deleted,decremented, no action\n";

while ( $line = from_json(<$fh>)){
    my $resources = $line->{ $resourcename.'s' };
    my $securitytoken = $line->{ securitytoken };
    my $nbDeleted = 0;
    my $nbNoAction = 0;
    my $nbDecremented = 0;
    my $timeperinput;
    my $total=DateTime::Duration->new();
    database->{ AutoCommit } = 0;
    my $begin = DateTime->now;
    #browse all the resources in the file
    foreach my $resource ( @{ $resources } ){
        my $storedResource;
        $nb++;

        #printable informations
        if ( ($nb % 1000 == 0) or $nb==1){
            my $duration = DateTime->now - $begin;
            $begin = DateTime->now;
            $total+=$duration;
            $timeperinput = ($duration->minutes * 60 + $duration->seconds);
            my ( $hours, $minutes, $seconds )= $total->in_units('hours','minutes','seconds');
            my $displayduration =  $hours."h".$minutes."m".$seconds."s";
            print "$nb,$displayduration,$timeperinput,$nbDeleted,$nbDecremented,$nbNoAction\n";
        }

        #commit all 1000 import ( performances )
        if ( $nb % 1000 == 0 and $autocommit ){ database->commit }
        $resource->{resource} = $resourcename;
        $storedResource =  Mana::Resource::search ( $resource );
        if ( $storedResource ){
            $storedResource = $storedResource->{data}->[0];
        }
        #if the file incremented the resource is a resource...
        if ( $storedResource and $storedResource->{nb} > 1 ){
            #then decrement it 
            my $params;
            $params->{resource} = $resourcename;
            $params->{field} = 'nb';
            my $step = $resource->{nb} || 1;
            $params->{step} = 0 - $step;
            $params->{id} = $storedResource->{id};
            Mana::Resource::incrementField( $params );
            $nbDecremented++;
        }
        elsif ( $storedResource and $storedResource->{nb} == 1){
            #else if it created a new resource
            #then delete the data in the database
            Mana::Resource::delete( $resourcename, $storedResource->{id});
            $nbDeleted++;
        }
        else{
            #else (mostly if the resource has been deleted in the meantime)
            $nbNoAction++;
        }
    }

    open(my $fh2, '>>:encoding(UTF-8)', $logfile) or (die "Could not open file '$logfile'");
    $return = "[".DateTime->now()->strftime("%a %D%k h%Mm%Ss"). " ] Deleting : $resourcename from $filename in DB, Nb lines before insert: $nbbeforeinsert, Deleted: $nbDeleted, Decremented: $nbDecremented, noaction: $nbNoAction\n";
    print $fh2 $return;
    close $fh2
}
close $fh;

if ( $autocommit ){
    database->commit;
}
else{
    warn "If you run this with option --confirm, the output will be as following".$return;
    database->rollback;
}

database->{ autocommit } = 1;

print $return;
