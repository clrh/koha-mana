#!/usr/bin/perl

# This file is part of Mana.
#
# Mana is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Koha is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Koha; if not, see <http://www.gnu.org/licenses>.

use Modern::Perl;


use Dancer2;
use Dancer2::Serializer::JSON;

use Mana::Resource;
use Dancer2::Plugin::Database;

use Getopt::Long;

=head1 NAME

Import Resources - Import any type of resources from a file

=head1 SYNOPSIS

PLACK_ENV=production import_resource.pl -f filename -r resourcename -l logfile

=head1 DESCRIPTION

This script imports resources from a file stored as JSON which 2 arguments:
- securitytoken: the security token of the sharer.
- [ resourcename ]: the resources which are about to be imported, the number of resources is not limited.

=head1 OPTIONS

=over

=item --filename

The name of the file you want to import in the database.

=item --resource

The name of the resource you want to import.

=item --logfile

The file where you want to store the logs ( default production.log ).

=back

=head1 RETURNED VALUE

Returns the string stored in the logs.

=cut

my $resourcename;
    #path is [mana path]/data/[resourcename]/
    #path for imported datas: [mana path]/data/[resourcename]/old/
my $nb = 0;
my $return;

my $filename;
my $logfile;
my $nbbeforeinsert = database->quick_count("reading_pair", 1);

my ($help, $language, $verbose);
GetOptions(
    'help|?'        => \$help,
    'language|l:s'  => \$language,
    'verbose|v'     => \$verbose,
    'filename|f:s'    => \$filename,
    'resource|r:s'    => \$resourcename,
    'logfile|l:s'       => \$logfile
) or pod2usage(2);


$logfile||=config->{ log_path }."import.log";

my $duration;
open(my $fh, '<:encoding(UTF-8)', $filename) or (die "Could not open file '$filename'");
my $line;
print "total,duration,timeper1000input,time per input,added,incremented,failed,existsbutfail\n";

while ( $line = from_json(<$fh>)){
    my $resources = $line->{ $resourcename.'s' };
    my $securitytoken = $line->{ securitytoken };
    my $result;
    my $result2;
    my $nbadded = 0;
    my $nbfailed = 0;
    my $nbincremented = 0;
    my $nbexistsnotincremented = 0;
    my $timeperinput;
    my $total=DateTime::Duration->new();
database->{ AutoCommit } = 0;
my $begin = DateTime->now;
    #browse all the resources in the file
    foreach my $resource ( @{ $resources } ){
        $nb++;

        #printable informations
        if ( ($nb % 1000 == 0) or $nb==1){
            my $duration = DateTime->now - $begin;
            $begin = DateTime->now;
            $total+=$duration;
            $timeperinput = ($duration->minutes * 60 + $duration->seconds);
            my ( $hours, $minutes, $seconds )= $total->in_units('hours','minutes','seconds');
            my $displayduration =  $hours."h".$minutes."m".$seconds."s";
            print "$nb,$displayduration,$timeperinput,$nbadded,$nbincremented,$nbfailed,$nbexistsnotincremented\n";
        }

        #commit all 1000 import ( performances )
        if ( $nb % 1000 == 0){ database->commit }
        $resource->{securitytoken} = $securitytoken;
        $result = Mana::Resource::postEntity( $resourcename, $resource);

            #if the pair has been added (status_created), consider it as a success
        if ( $result->{ statuscode } == 201 ){
            $nbadded++;
        }

            #if the pair hasn't been added (status_already_exists), try to increment the number
        elsif ( $result->{ statuscode } == 208 and ( $resourcename eq 'reading_pair') ){
            $resource->{resource} = $resourcename;
            $resource->{field} = 'nb';
            $resource->{step} = $resource->{nb};
            $resource->{id} = $result->{id};
            eval{ $result2 = Mana::Resource::incrementField( $resource ) };

            #if the increment worked
            if ($result2->{ statuscode} == 200 ){
                $nbincremented++;
            }

            #if the increment didn't work
            else{
                $nbexistsnotincremented++;
            }
        }

            #if the pair hasn't been added for another reason considers it as failed
        else {
            $nbfailed++;
        }
    }
    
    open(my $fh2, '>>:encoding(UTF-8)', $logfile) or (die "Could not open file '$logfile'");
    $duration = DateTime->now - $begin;
    $total+=$duration;
    $return = "[".DateTime->now()->strftime("%a %D%k h%Mm%Ss"). " ], Duration (s): ". $total->seconds . ", Importing: $resourcename, From: $filename in DB, Nb lines before insert: $nbbeforeinsert, Failed: $nbfailed, Added: $nbadded, Incremented: $nbincremented, Existing but not incremented: $nbexistsnotincremented\n";
    print $fh2 $return;
    close $fh2
}
close $fh;

database->commit;
database->{ autocommit } = 1;

print $return;
