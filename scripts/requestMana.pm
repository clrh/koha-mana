use Modern::Perl;

use HTTP::Request::Common;
use LWP::UserAgent;
use Dancer2;

# $ARGV[0] -> route
# $ARGV[1] -> file

=head1 NAME

requestMana - request mana wit one file as content

=head1 DESCRIPTION

This script allows you to send one file to mana ( otherwise you can use http requester )

=cut

my $file = @ARGV[1];

open(my $fh, '<:encoding(UTF-8)', $file) or die "Could not open file '$file'";
my $line = <$fh>;

my $ua = LWP::UserAgent->new;
$ua->timeout(100000000);


my $url = config->{mana_domain}.$ARGV[0];

my $request = HTTP::Request->new( POST => $url );
$request->content($line);
my $result = $ua->request( $request ); 
close $fh;
