#!/usr/bin/perl

# This file is part of Koha.
#
# Koha is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Koha is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Koha; if not, see <http://www.gnu.org/licenses>.

=head1 NAME

export_subscriptions_for_mana.pl - Export subscription to Mana format.

=head1 SYNOPSIS

./ export_subscriptions_for_mana.pl -l fr-FR

=head1 DESCRIPTION

This script export all subscription into a CSV file intended
to be imported into Mana server.

=cut

use Modern::Perl;
use Getopt::Long;
use C4::Context;
use Pod::Usage;
use Text::CSV;

my ($help, $language, $verbose);
GetOptions(
    'help|?'        => \$help,
    'language|l:s'  => \$language,
    'verbose|v'     => \$verbose,
) or pod2usage(2);

$language ||= 'en';

=head1 NAME

export_subscriptions_for_mana.pl - Export subscription to Mana format.

=head1 SYNOPSIS

export_subscriptions_for_mana.pl
  [ -l ]

=head1 OPTIONS

=over 8

=item B<--help>

Print a brief help message and exits.

=item B<--language>

Subscription language that will be specified into Mana.

=back
=cut

my $dbh = C4::Context->dbh;
my $sth = $dbh->prepare("SELECT savedsql, report_name, notes, report_group type FROM saved_sql") or die $dbh->errstr;

my @headers = ('savedsql', 'report_name', 'notes', 'type', 'report_group', 'kohaversion', 'language');

my $content = join(',', @headers) . "\n";

my %versions = C4::Context::get_versions();

$sth->execute;
my $results = $sth->fetchall_arrayref({});
foreach my $r ( @$results ) {
    $r->{kohaversion} = $versions{'kohaVersion'};
    $r->{language} = $language;
    foreach my $header ( @headers ) {
        $r->{ $header } ||= '';
        $content .= '"' . $r->{ $header } . '",';
    }
    $content =~ s/\,$/\n/;
}
print $content;
