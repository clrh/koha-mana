$(document).ready( function(){
    $("#resource_data_table").on( "click", ".delete", function(){
        var confirmation = confirm("Are you sure you want to delete this resource ?");
        if ( confirmation == true ) {
            var id = $(this).parents("tr").children(".id").text();
            var resource = $("#resourcetype").val();
            var myurl = '/admin/' + resource + '/delete/' + id;
            $.ajax({
                type: "POST",
                url: myurl,
                dataType: "json",
            }).done( function() {
                var row = ("#row").concat(id);
                $(row).hide();
            }).fail(function(jqXHR, msg, longmsg){
            });
        }
    });

    $("#resource_data_table").on( "click", ".show", function(){
        var id = $(this).parents("tr").children(".id").text();
        var resource = $("#resourcetype").val();
        var myurl = "/admin/details/".concat(resource).concat(".").concat(id);
        $.ajax({
            type: "GET",
            url: myurl,
            data: { usecomments: 1 }
        }).done( function( result ) {
            $(".modal-body").html( result );
            $("#detailstable").modal('show');
        }).fail( function( jqXHR, msg, longmsg ){
        });
    });
});
