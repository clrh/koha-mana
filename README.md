# Mana server

## Sharing data between Koha installations

This webservice enables the sharing of information between different instances of Koha, like subscription numbering parterns, subscription frequencies, and SQL reports.

### External references

 * [Mana project RFC](https://wiki.koha-community.org/wiki/Recommendation_engine_integrated_with_Mana_project_RFC) on the Koha community wiki
 * [Mana documentation](https://wiki.koha-community.org/wiki/Mana_central_database)
 * [Bug 17047](https://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=17047) - implement Mana (sharing subscriptions and reports) on Koha
 * [Bug 18922](https://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=18922) - add the ability to share comments (reviews)

### Prerequisites

```
apt install libdancer2-perl libdancer2-plugin-database-perl libdancer-logger-psgi-perl cpanminus libemail-mime-perl libemail-sender-perl libbytes-random-secure-perl
cpanm Dancer2::Plugin::REST
cpanm Dancer2::Plugin::Passphrase
cpanm Dancer2::Plugin::Auth::Tiny
cpanm Dancer2::Logger::Syslog
```

### Installing

```
git clone git@git.biblibre.com:biblibre/koha-mana.git
mysql -e "CREATE DATABASE mana CHARSET=utf8 COLLATE=utf8_unicode_ci;"
cd koha-mana
mysql mana < sql/schema.sql
cp environments/config.yml environments/production.yml
#In this production.yml file, replace all <REPLACE_ME> with the name of the database created above and a user/password
```

## Mana UI

Mana UI is the web interface that allows you to quickly explore the databases.

### Launch the Mana UI

```
$ plackup -r -R lib -E production bin/mana-ui.psgi -l 0.0.0.0:5000
```

### Test

You can enable the demo mode by editing config.yml

```
demo: 1
```

So you can authenticate yourself by using demo/demo.

## Mana API

This is the part that Koha communicates with.

### Launch Mana API

```
$ plackup -r -R lib -E production bin/mana.psgi -l 0.0.0.0:5001
```

### Test

#### Create a librarian (get a security token)
First, create a librarian to get a security token. A librarian is allowed to share Koha entities (subscription, report etc...):
```
curl -X POST "0.0.0.0:5001/getsecuritytoken" -H 'Content-Type: application/json' -d'{"firstname": "Joe", "lastname": "Dassin", "email": "bar@bar.com"}'
```

You should get a response like this:
```
{"token":"liOqTlWRegmZPEGEOBQ9jQ8verqZchMG"}
```

Enable the librarian. You can do it directly by using a mysql command:
```
mysql mana -e "UPDATE librarian SET activationdate = '2018-07-02'"
```

#### Share

Now you can share things on Mana. This call shares a report:
```
curl -X POST "0.0.0.0:5001/report.json" -H 'Content-Type: application/json' -d'{"savedsql": "SELECT count(*) FROM biblio", "report_name": "A report", "notes": "this is a test report", "exportemail": "xx@xx.com", "kohaversion": "17.12.00.021", "language": "en", "report_group": "Catalogue", "securitytoken": "0NTvjzK8jK3NGLIhpnH2S4U3aiUBjRiR"}'
```

#### Search

Here we search the report:
```
curl -X GET "0.0.0.0:5001/report.json" -H 'Content-Type: application/json' -d'{"report_name": "A report"}'
```

#### Routes

Here is the list of available routes in Mana:
```
# resource can be: subscription, report or review.
get '/resource/:id.json' # Get a subscription by its id

get '/resource.json' # Search resources

post '/:resource/:id.:format/increment/:field' # Increment a field (i.e. nbofusers) for a resource

post '/:resource.:format' Share a resource with Mana

post '/bulk/:resource.:format' Same than the previous one but allow to share many resources at one time

post '/getsecuritytoken' # Create a librarian and retourne a security token

```

## Built With

 * [Dancer2](http://perldancer.org/) - powerful web application framework for Perl

## Authors

 * Baptiste Wojtkowski <baptiste.wojtkowski@biblibre.com>
 * Alex Arnaud <alex.arnaud@biblibre.com>
